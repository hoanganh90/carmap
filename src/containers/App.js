import React, { Component } from 'react'
import SideBar from './SideBar'
import MapContainer from './Map'
import carLocations from "./locations.json";

class App extends Component {
  render() {
    return (
      <div className="app">
        <MapContainer
          center={{ lat: 53.591181038314915, lng: 9.999211999316572 }}
          zoom={11}
          data={carLocations.placemarks}
        />
        <SideBar
          data={carLocations.placemarks} width={300} height={"100vh"}
        />
      </div>
    )
  }
}
export default App