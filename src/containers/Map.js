import React, { Component } from "react";
import { Map, Marker, InfoWindow, GoogleApiWrapper } from "google-maps-react";
import userloc from '../assets/user.svg';
export class MapContainer extends Component {
  state = {
    activeMarker: {},
    selectedPlace: {},
    showingInfoWindow: false,
    maps: this.props.data.map(obj => ({ ...obj, isVisible: true })),
    userLocation: { lat: 0, lng: 0 },
    loading: true,
    isAllMarkerShowed: true
  };
  getLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords;
        this.setState({
          userLocation: { lat: latitude, lng: longitude },
          loading: false
        });
      },
      () => {
        this.setState({ loading: false });
      }
    );
  }

  onMarkerClick = (props, marker) => {
    var tempMap = this.state.maps
    if (!this.state.isAllMarkerShowed) {//Enable all markers and disable InforWindow
      tempMap.forEach((element, index) => {
        tempMap[index].isVisible = true;
      });
      this.setState({
        activeMarker: null,
        showingInfoWindow: false,
        isAllMarkerShowed: true
      });
    } else {
      tempMap.forEach((element, index) => {
        if (element.name !== marker.name) {
          tempMap[index].isVisible = false;
        }
      });
      this.setState({
        activeMarker: marker,
        selectedPlace: props,
        showingInfoWindow: true,
        maps: tempMap,
        isAllMarkerShowed: false
      });
    }

  }
  onInfoWindowClose = () => {
    var tempMap = this.state.maps
    tempMap.forEach((element, index) => {
      tempMap[index].isVisible = true;
    });
    this.setState({
      activeMarker: null,
      showingInfoWindow: false,
      maps: tempMap,
      isAllMarkerShowed: true
    });
  }
  onMapClicked = () => {
    if (this.state.showingInfoWindow) {
      var tempMap = this.state.maps
      tempMap.forEach((element, index) => {
        tempMap[index].isVisible = true;
      });
      this.setState({
        activeMarker: null,
        showingInfoWindow: false,
        isAllMarkerShowed: true
      });
    }
  };
  render() {
    const google = window.google
    const data = this.state.maps;
    this.getLocation() 
    const userLocation = this.state.userLocation
    return (
      <div className="map-container">
        <Map
          google={this.props.google}
          className={"map"}
          zoom={this.props.zoom}
          initialCenter={this.props.center}
          onClick={this.onMapClicked}
        >
          {data.map(item => (
            <Marker onClick={this.onMarkerClick}
              title={item.name}
              name={item.name}
              position={{ lat: item.coordinates[1], lng: item.coordinates[0] }}
              visible={item.isVisible}
            />
          ))}
          <InfoWindow
            marker={this.state.activeMarker}
            onClose={this.onInfoWindowClose}
            visible={this.state.showingInfoWindow}
          >
            <div>
              <h4>{this.state.selectedPlace.name}</h4>
            </div>
          </InfoWindow>
          <Marker onClick={this.onMarkerClick}
            position={userLocation}
            visible={true}
            icon={{
              url: userloc,
              anchor: new google.maps.Point(5, 5),
            }}
          />
        </Map>
      </div>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyCLEEQpyCpADg9ohtUq_81pnRAx-Bk8Bso"
})(MapContainer);