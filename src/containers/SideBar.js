import React from "react";
import './SideBar.css';
import CarList from '../components/CarList'

export const SideBar = ({ width, height, data }) => {
  const [xPosition, setX] = React.useState(-width);

  const toggleMenu = () => {
    if (xPosition < 0) {
      setX(0);
    } else {
      setX(-width);
    }
  };
  React.useEffect(() => {
    setX(0);
  }, []);
  return (
    <div
      className="side-bar"
      style={{
        transform: `translatex(${xPosition}px)`,
        width: width,
        minHeight: height
      }}
    >
      <CarList cars={data} ></CarList>
      <button
        onClick={() => toggleMenu()}
        className="toggle-menu"
        style={{
          transform: `translate(${width}px, 20vh)`
        }}
      ></button>
    </div>
  );
};
export default SideBar