import React from 'react'
const SearchBox = ({searchfiled,searchChange}) => {
    return (
        <div>
            <input
                className='searchbox'
                type='search'
                placeholder='Search cars'
                onChange={searchChange}  
            />
        </div>
    )
}
export default SearchBox