import React from 'react'
import './Car.css'
class Car extends React.Component {
    render() {
        const { address, coordinates, engineType, exterior, fuel, interior, name, vin } = this.props.car;
        return (
            <div class="dropdown" >
                <button class="dropbtn">{name}-VIN: {vin}</button>
                <div class="dropdown-content">
                    <a href="#">Address: {address}</a>
                    <a href="#">Coordinates: {coordinates[0]} - {coordinates[1]} </a>
                    <a href="#">EngineType: {engineType}</a>
                    <a href="#">Exterior:{exterior}</a>
                    <a href="#">Fuel: {fuel}</a>
                    <a href="#">Interior: {interior}</a>
                </div>
          </div>
        )
    }
}

export default Car;