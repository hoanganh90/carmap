import React from 'react';
import Car from './Car';
import SearchBox from './SearchBox';
import Scroll from './Scroll'
class CarList extends React.Component {
    constructor() {
        super()
        this.state = {
            searchfield: ''
        }
    }
    onSearchChange = (event) => {
        this.setState({ searchfield: event.target.value })
    }

    render() {
        const cars = this.props.cars
        const searchfield  = this.state.searchfield
        const filteredCars = cars.filter(car => {
            return (car.name.toLowerCase().includes(searchfield.toLowerCase())
                    ||car.address.toLowerCase().includes(searchfield.toLowerCase())
                    ||car.engineType.toLowerCase().includes(searchfield.toLowerCase())
                    ||car.vin.toLowerCase().includes(searchfield.toLowerCase())
                    ||car.interior.toLowerCase().includes(searchfield.toLowerCase()))
                })
        return (
            <div>
                <h3>Number of cars: {this.props.cars.length}</h3>
                <SearchBox searchChange={this.onSearchChange}></SearchBox>
                <Scroll>
                    {filteredCars.map(car => (
                        <Car
                            car={car}
                        />
                    ))}
                </Scroll>
            </div>
        )
    }
}
export default CarList